import java.util.Scanner;

public class Roulette{
  public static void main(String[] args){
    RouletteWheel rw = new RouletteWheel();
    int userMoney = 1000;
    Scanner read = new Scanner(System.in);
    int bet = 0;
    int moneyBet = 0;
    int changes = 0;
    int totalChanges = 0;

    System.out.println("Wish to make a bet? (yes/no)");
    String dec = read.next();

    while(dec.equals("yes") || dec.equals("YES") || dec.equals("Yes")) {
      if(userMoney > 0) {
        bet = numberBeingBet(bet, read);
        rw.spin();
        moneyBet = amountBeingBet(moneyBet, read, userMoney);
        changes = checkResult(bet, rw, moneyBet);
        userMoney += changes;
        totalChanges += changes;
        System.out.println("Money left: " + userMoney);
        System.out.println("Wish to bet again?");
        dec = read.next();
      }
    }

    if(dec.equals("no") || dec.equals("NO") || dec.equals("No")) {
      System.out.println("You chose not to bet.");
      System.out.println("Total amount of money won: " + totalChanges);
    }
  }

  public static int numberBeingBet(int bet, Scanner read) {
    System.out.println("Which number do you want to bet on?");
    bet = read.nextInt();
    while(bet > 36 || bet < 0) {
      System.out.println("Invalid input. Pick a number from 0 to 36.");
      bet = read.nextInt();
    }
    return bet;
  }

  public static int amountBeingBet(int moneyBet, Scanner read, int userMoney) {
    System.out.println("How much do you want to bet? Please bet a whole number.");
    moneyBet = read.nextInt();
    while(moneyBet > userMoney || moneyBet <= 0) {
      System.out.println("Sum is either too large, negative or null. Try again.");
      moneyBet = read.nextInt();
    }
    return moneyBet;
  }

  public static int checkResult(int bet, RouletteWheel rw, int moneyBet) {
    int change;
    if(bet == rw.getValue()) {
      System.out.println("You won!");
      change = moneyBet*35;
      return change;
    } 
    System.out.println("You lost.");
    change = moneyBet*(-1);
    return change;
  }
}