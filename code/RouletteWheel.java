import java.util.Random;

public class RouletteWheel {
  private Random r;
  private int lastSpin;

  public RouletteWheel() {
    this.r = new Random();
    this.lastSpin = 0;
  }

  public void spin() {
    final int BOUND = 37;
    this.lastSpin = this.r.nextInt(BOUND);
  }

  public int getValue() {
    return this.lastSpin;
  }
}
